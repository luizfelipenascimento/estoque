<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Fornecedor;

class FornecedorController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth');
    }

    public function index() {
        $fornecedores = Fornecedor::all();
        return view('fornecedor.index', compact('fornecedores'));
    }

    public function showCreateForm() {
        return view('fornecedor.edit');
    }

    public function store(Request $request) {
        try {
            Fornecedor::create([
                'name' => $request->name,
                'cnpj' => $request->cnpj,
                'endereco' => $request->endereco            
            ]);
        } catch(Exception $e) {
            $request->session()->flash('alert-class', 'alert-danger');
            $request->session()->flash('message', 'Falha ao cadastrar o fornecedor');        
            return view('fornecedor.edit');
        }
        
        $request->session()->flash('alert-class', 'alert-success');
        $request->session()->flash('message', 'Fornecedor cadastrado com sucesso');
        return $this->index();
    }

    public function search(Request $request, $find = null) {
        $find = ($find == null) ? $request->find : $find;    
        $fornecedores = Fornecedor::where('name','like', $find.'%')->orWhere('cnpj',$find)->get();            
        if($fornecedores->count() >= 1) {
            return view('fornecedor.index', compact('fornecedores'));
        }
    }

    public function edit(Request $request, $id) {
        $fornecedor = Fornecedor::find($id);
        $fornecedor->name = $request->name;
        $fornecedor->cnpj = $request->cnpj;
        $fornecedor->endereco = $request->endereco;
        if($fornecedor->save())

        $fornecedor = Fornecedor::find($id);
        return view('fornecedor.edit',compact('fornecedor'));
    }   

    public function showEditForm($id) {
        $fornecedor = Fornecedor::find($id);
        return view('fornecedor.edit',compact('fornecedor'));
    }

    public function deleteFornecedor(Request $request, $id) {
        $msg;
        $fornecedor = Fornecedor::find($id);
        
        if($fornecedor->produtos->isEmpty()) {
            $fornecedor->delete();
        } else  $msg = "Falha ao deletar o fornecedor: O fornecedor possui produtos cadastrados";

        if ($fornecedor->trashed()) {
            setMessageAlerts($request, "Fornecedor deletado com sucesso","alert-success");
        } else {
            setMessageAlerts($request, ($msg) ? $msg : "Falha ao deletar o fornecedor","alert-danger");
        }
        
        return $this->index();
    }
}
