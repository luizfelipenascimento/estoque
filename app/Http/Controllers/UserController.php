<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;

class UserController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }    

    public function index() {
        if(Auth::user()->tipo != 1) return abort('403');
        $usuarios = User::all();
        return view('user.index', compact('usuarios'));
    }

    public function store(Request $request) {
        if(Auth::user()->tipo != 1) return abort('403');
        //$this->validate();
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'tipo' => $request->tipo,
            'status' => 1
        ]);
        $request->session()->flash('alert-class', 'alert-success');
        $request->session()->flash('message', 'Usuario cadastrado com sucesso');        

        return $this->index();
    }

    public function showCreateForm() {
        return view('user.edit');     
    }

    public function showEditForm($id) {
        $usuario = User::find($id);
        return view('user.edit', compact('usuario'));
    }

    public function edit(Request $request, $id) {
        //$this->validate();
        
        $usuario = User::find($id);
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = $request->password == '' ? $usuario->password : bcrypt($request->password);
        $usuario->tipo = $request->tipo;
        $usuario->status = !isset($request->status) ? $usuario->status : $request->status;

        if($usuario->save()) {
            $request->session()->flash('message', 'Usuario alteradado com sucesso');
            $request->session()->flash('alert-class', 'alert-success');
        } else {
             $request->session()->flash('message', 'Falha ao alterar o usuario');
            $request->session()->flash('alert-class', 'alert-danger');
        }
        

        return view('user.edit', compact('usuario'));
    }

    /*public function validate($request) {
        return Validator::make($request, [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'tipo' => 'required',
            'status' => 'required'
        ]);
    }*/


}
