<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Fornecedor;

class ProdutoController extends Controller
{
    
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $produtosQtdZero = Produto::where('qtd', '=',0)->get();
        $produtos = Produto::where('qtd', '>',0)->get();
        
        $fornecedores = Fornecedor::all();
        return view('produto.index', compact('produtos', 'fornecedores', 'produtosQtdZero'));
    }

    public function showCreateForm() {
        $fornecedores = Fornecedor::all();
        return view('produto.edit', compact('fornecedores'));
    }


    public function store(Request $request) {
        try {
            Produto::create([
                'name' => $request->name,
                'descricao' => $request->descricao,
                'preco' => $request->preco,
                'fornecedor_id' => (int) $request->fornecedor,
                'qtd' => $request->qtd
            ]);
        } catch(Exception $e) {
            setMessageAlerts($request, 'Erro ao cadastrar o produto', 'alert-danger');    
            return view('produto.edit');
        }

        setMessageAlerts($request, 'Produto cadastrado com sucesso', 'alert-success');
        return $this->index();
    }

    public function edit(Request $request, $id) {
        $produto = Produto::find($id);
        $produto->name = $request->name;
        $produto->descricao = $request->descricao;
        $produto->preco = $request->preco;
        $produto->fornecedor_id = (int) $request->fornecedor;
        $produto->qtd = $request->qtd;
        if($produto->save()) {
            setMessageAlerts($request, 'Produto alterado com sucesso', 'alert-success');
        } else  setMessageAlerts($request, 'Produto falha ao alterar o produto', 'alert-success');
        
        $fornecedores = Fornecedor::all();   
        return view('produto.edit', compact('fornecedores', 'produto'));
    }

    public function search(Request $request) {
        $produtos;
        if($request->fornecedor == -1 || !isset($request->fornecedor)) 
            $produtos = Produto::where('name','like', $request->find.'%')->get();
        else $produtos = Produto::where('fornecedor_id','=', $request->fornecedor)->get();
        
        $fornecedores = Fornecedor::all();
        if($produtos->count() >= 1) {
            return view('produto.index', compact('produtos', 'fornecedores'));
        }
    }

    public function showEditForm($id) {
        $produto = Produto::find($id);
        $fornecedores = Fornecedor::all();
        return view('produto.edit', compact('produto', 'fornecedores'));
    }
}
