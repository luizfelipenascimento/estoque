<?php

use Illuminate\Http\Request;

//str_replace(",", "&amp;", str_replace("'", "&#039", $msg));
function escapeString($msg) {
    return str_replace("'", "&#039;", $msg);   
}

function setMessageAlerts($request, $msg, $class) {
    $request->session()->flash('alert-class', $class);
    $request->session()->flash('message', $msg);
}
