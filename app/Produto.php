<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fornecedor;

class Produto extends Model
{
    protected $table = "produto";


    protected $fillable = ['name', 'descricao', 'preco', 'fornecedor_id', 'qtd'];

    public function fornecedor() {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

}
