<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'tipo' => rand(0,1),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'status' => 1
    ];
});

$factory->define(App\Fornecedor::class, function (Faker\Generator $faker) {
    return [    
        'name' => $faker->company,
        'endereco' =>  $faker->streetAddress,
        'cnpj' => $faker->regexify('[0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\/00001-[0-9][0-9]')
    ];
});

$factory->define(App\Produto::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->word,
        'descricao' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'preco' => rand(1, 10000),
        'qtd' => rand(1, 15),
        'fornecedor_id' => factory('App\Fornecedor')->create()->id,
    ];
});