@extends('layouts.app')

@section('content')
<div class="panel-heading"><h2>{{Request::is('users/create') ? 'Casdastrar Usuario' : 'Editar Usuario'}}</h2></div>
<div class="panel-body">
    @if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
	@endif
    <form action="@if(Request::is('users/create')) /users/create @else /users/edit/{{$usuario->id}} @endif" 
        method="post" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="col-md-3 control-label">Nome: </label>
            <div class="col-md-8">
                <input class="form-control" type="text" name="name" value="{{ isset($usuario) ? $usuario->name : ''}}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Email: </label>
            <div class="col-md-8">
                <input class="form-control" type="email" name="email" value="{{ isset($usuario) ? $usuario->email : ''}}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Role:</label>
            <div class="col-md-8">
                <select name="tipo" class="form-control">
                    <option value="1" 
                        @if(isset($usuario) && $usuario->tipo == 1 ) selected="selected" @endif> 
                        Administrador
                    </option>
                    <option value="0" 
                        @if(isset($usuario) && $usuario->tipo == 0 ) selected="selected" @endif>
                        Colaborador
                    </option>
                </select>
            </div>
         </div>
       
        <div class="form-group">
            <label for="password" class="col-md-3 control-label">Password</label>
            <div class="col-md-8">
                <input id="password" type="password" class="form-control" name="password" value="" 
                    placeholder="{{ isset($usuario) ? 'Digite a nova senha caso deseje alterar' : '' }}">
            </div>
        </div>
        
        @if(Request::is('users/edit/*'))
            <div class="form-group">
                <label class="col-md-3 control-label">Status: </label>
                <div class="col-md-8">
                    <div class="form-control">{{ ($usuario->status == 1) ? "Usuario Ativado" : "Usuario Desativado"}}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">{{$usuario->status == 1 ? 'Desativar:' : 'Ativar:'}} </label>
                <div class="col-md-1 ">
                    @if($usuario->status == 1)
                        <input class="form-control" type="checkbox" name="status" value="0"> 
                    @else
                        <input class="form-control" type="checkbox" name="status" value="1">
                    @endif  
                </div>
            </div>
        @endif
      
        <div class="form-group">
            <div class="col-md-6 col-md-offset-6">            
                <input type="submit" class="btn btn-primary" 
                    value="{{Request::is('users/create') ? 'Create' : 'Update'}}"/>
            </div>
        </div>
    </form>
</div>
@endsection