@extends('layouts.app')

@section('content')
<div class="panel-heading"><h2>Usuarios</h2></div>
<div class="panel-body">
	@if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
	@endif
	<a href="/users/create" class="btn btn-primary pull-right">Adicionar Usuario</a>
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Nome</th>
			<th>Email</th>
			<th>Função</th>
			<th>Status</th>
		</thead>
		<tbody>
			@foreach($usuarios as $usuario)
				<tr>
					<td><a href="users/edit/{{$usuario->id}}">{{$usuario->id}}</a></td>
					<td>{{ $usuario->name }}</td>
					<td>{{ $usuario->email }}</td>
					<td> 
						{{ $usuario->tipo == 1 ? "Administrador" : "Colaborador"}}
					</td>
					<td>{{ $usuario->status == 1 ? "Ativado" : "Desativado"}} </td>	
				</tr>
			@endforeach	
		</tbody>
	</table>
</div>
@endsection