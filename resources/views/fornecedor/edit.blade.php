@extends('layouts.app')
@section('content')
<div class="panel-heading"><h2> {{ Request::is('fornecedor/create') ? 'Criar Fornecedor' : 'Editar Fornecedor' }}</h2></div>
<div class="panel-body">
    @if(Session::has('message'))
        {{ Session::get('message') }}
    @endif
    <form action="@if(Request::is('fornecedor/create')) /fornecedor/create @else /fornecedor/edit/{{$fornecedor->id}} @endif" 
        class="form-horizontal" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        <div class="form-group">
            <label class="col-md-3 control-label">Nome: </label>
            <div class="col-md-8">
                <input class="form-control" name="name" value="{{ isset($fornecedor) ? $fornecedor->name : ''}}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">CNPJ: </label>
            <div class="col-md-8">
               <input class="form-control" name="cnpj" value="{{ isset($fornecedor) ? $fornecedor->cnpj : ''}}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Endereço: </label>
            <div class="col-md-8">    
                <input class="form-control" name="endereco" value="{{ isset($fornecedor) ? $fornecedor->endereco : ''}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-6">            
                <input type="submit" class="btn btn-primary" value="{{Request::is('fornecedor/create') ? 'Create' : 'Update'}}"/>
            </div>
        </div>
    </form>
</div>
@endsection