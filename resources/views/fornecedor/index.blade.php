@extends('layouts.app')

@section('content')
<div class="panel-heading"><h2>Fornecedores</h2></div>
<div class="panel-body">
	@if(Session::has('message') && Session::has('alert-class'))
		<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
	@endif
    <form method="get" class="form-group" action="/consulta/fornecedor">
		<div class="col-md-4">
			<input name="find" class="form-control" placeholder="Buscar">
		</div>
		<input class="btn btn-primary" type="submit" value="Buscar">
    </form>
	<div>
		<a href="/fornecedor/create" class="btn btn-primary pull-right">Cadastrar Fornecedor</a>
	</div>
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Nome</th>
			<th>Cnpj</th>
			<th>Endereço</th>
			<th>Deletar</th>
		</thead>
		<tbody>
			@foreach($fornecedores as $fornecedor)
				<tr>
					<td><a href="fornecedor/edit/{{$fornecedor->id}}">{{ $fornecedor->id }}</td>
					<td>{{ $fornecedor->name }}</td>
					<td>{{ $fornecedor->cnpj }}</td>
					<td>{{ $fornecedor->endereco }} </td>		
					<td><a id="deleta_{{$fornecedor->id}}"
							href="/fornecedor/delete/{{$fornecedor->id}}" class="btn btn-danger">deletar</a></td>
				</tr>
			@endforeach	
		</tbody>
	</table>
</div>
@endsection