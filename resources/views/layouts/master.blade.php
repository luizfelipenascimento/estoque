<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="simple blog">
        <meta name="author" content="Luiz Felipe">
        <link rel="icon" href="favicon.ico">

        <title>Sistema de estoque</title>

        <!-- Bootstrap core CSS -->
        <link href="/public/css/bootstrap.min.css" rel="stylesheet">
        <link href="/public/css/app.css" rel="stylesheet">
        

        
    </head>
    <body>
        <div class="container">
            @yield('content')            
        </div>
        @include('partials.footer')
    </body>
</html>