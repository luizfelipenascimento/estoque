@extends('layouts.app')

@section('content')
<div class="panel-heading">Dashboard</div>
<div class="panel-body">
    @if(Auth::user()->tipo == 1)
        <span> Valor total dos produtos: {{$total}} </span>
    @endif
</div>
@endsection
