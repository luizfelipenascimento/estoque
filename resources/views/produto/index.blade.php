@extends('layouts.app')

@section('content')
<div class="panel-heading"><h2>Produtos</h2></div>
<div class="panel-body">
    @if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
	@endif
    <form method="get" class="form-group" action="/consulta/produto">
        <div class="col-md-4">
            <input name="find" class="form-control" placeholder="Buscar">
        </div>
        <div class="col-md-4">
            <select name="fornecedor" class="form-control">
                <option value="-1">Fornecedor</option>
                @foreach($fornecedores as $fornecedor)
                    <option value="{{$fornecedor->id}}">{{$fornecedor->name}}</option>
                @endforeach
            </select>
        </div>
        <input class="btn btn-primary" type="submit" value="Buscar">
    </form>
    <div>
        <a href="/produtos/create" class="btn btn-primary pull-right">Cadastrar Produto</a>
    </div>
    <table class="table">
        <caption><center><h4>Tabela dos Produtos em estoque</h4></center></caption>
        <thead>
            <th>id</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Fornecedor</th>
            <th>Qtd</th>
            <th>Preço</th>
        </thead>
        <tbody>
            @foreach($produtos as $produto)
                <tr>
                    <td><a href="/produto/edit/{{$produto->id}}">{{ $produto->id }}</a></td>
                    <td>{{ $produto->name }}</td>
                    <td>{{ $produto->descricao }}</td>
                    <td>{{ $produto->fornecedor->name }} </td>
                    <td>{{ $produto->qtd}}</td>
                    <td>{{$produto->preco}}</td>		
                </tr>
            @endforeach	
        </tbody>
    </table>

    @if(isset($produtosQtdZero))
        <table id="TableQtdZero" class="table" >
            <caption><center><h4>Tabela dos Produtos com quantidade igual a 0</h4></center></caption>
            <thead>
                <th>id</th>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Fornecedor</th>
                <th>Qtd</th>
                <th>Preço</th>
            </thead>
            <tbody>
                @foreach($produtosQtdZero as $produto)
                    <tr>
                        <td><a href="/produto/edit/{{$produto->id}}">{{ $produto->id }}</a></td>
                        <td>{{ $produto->name }}</td>
                        <td>{{ $produto->descricao }}</td>
                        <td>{{ $produto->fornecedor->name }} </td>
                        <td>{{ $produto->qtd}}</td>
                        <td>{{$produto->preco}}</td>		
                    </tr>
                @endforeach	
            </tbody>
        </table>
    @endif
</div>
@endsection