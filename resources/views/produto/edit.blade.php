@extends('layouts.app')

@section('content')
<div class="panel-heading"><h2>{{Request::is('produtos/create') ? 'Cadastrar Produto' : 'Editar Produto'}}</h2></div>
<div class="panel-body">
    @if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
	@endif
    <form method="post" class="form-horizontal"
            action="@if(Request::is('produtos/create')) /produtos/create @else /produto/edit/{{$produto->id}} @endif">
        
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label class="col-md-3 control-label">Nome: </label>
            <div class="col-md-8">
                <input name="name" class="form-control" value="{{isset($produto) ? $produto->name : ''}}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Descriação: </label>
            <div class="col-md-8">
                <input name="descricao" class="form-control" value="{{isset($produto) ? $produto->descricao : ''}}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Preço: </label> 
            <div class="col-md-6">
                <input name="preco" class="form-control" 
                    type="number" step="0.1" value="{{ isset($produto) ? round($produto->preco,1) : ''}}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Quantidade: </label> 
            <div class="col-md-3">
                <input name="qtd" class="form-control" value="{{isset($produto) ? $produto->qtd : ''}}">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Fornecedor: </label> 
            <div class="col-md-8">
                <select name="fornecedor" class="form-control">
                    @foreach($fornecedores as $fornecedor)
                        <option value="{{ $fornecedor->id }}"
                            @if(isset($produto))
                                @if($fornecedor->id == $produto->fornecedor->id) 
                                    selected="selected" 
                                @endif
                            @endif>
                            {{ $fornecedor->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-6">            
                <input type="submit" class="btn btn-primary" value="{{Request::is('produtos/create') ? 'Create' : 'Update'}}">
            </div>
        </div>
    </form>
</div>
@endsection