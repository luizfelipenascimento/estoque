<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Fornecedor;
use App\Produto;

class FornecedoresTest extends TestCase
{

    use DatabaseMigrations;

    /** @test */
    public function test_usuario_pode_adicionar_fornecedor() {
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        $fornecedor = factory(Fornecedor::class)->make();

        $response = $this->post('fornecedor/create', [
                'name' => $fornecedor->name,
                'cnpj' => $fornecedor->cnpj,
                'endereco' => $fornecedor->endereco                
            ]);
    
        $response->assertStatus(200);
        $response->assertSee(escapeString($fornecedor->name));
        $response->assertSee($fornecedor->cnpj);
        $response->assertSee(escapeString($fornecedor->endereco));
    }

    /** @test */
    public function test_usuario_pode_listar_fornecedores() {
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        $fornecedor = factory(Fornecedor::class)->create();

        $response = $this->get('/fornecedores');
        
        
        $response->assertStatus(200);
        $response->assertSee(escapeString($fornecedor->name));
        $response->assertSee($fornecedor->cnpj);
        $response->assertSee(escapeString($fornecedor->endereco));
    }   

    /** @test */
    public function test_usuario_pode_buscar_nome_fornecedores() {
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        $fornecedor = factory(Fornecedor::class)->create();
        
        $response = $this->post('fornecedores/'.$fornecedor->name);

        $response->assertStatus(200);
        $response->assertSee(escapeString($fornecedor->name));
        $response->assertSee($fornecedor->cnpj);
        $response->assertSee(escapeString($fornecedor->endereco));
    }

    /** @test */
    public function test_usuario_pode_editar_fornecedor() {
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        
        $fornecedor = factory(Fornecedor::class)->create();

        $response = $this->post('fornecedor/edit/'.$fornecedor->id, [
            'name' => 'disil',
            'cnpj' => '66.459.220/0001-96',
            'endereco' => $fornecedor->endereco
        ]);

        $fornecedorNew = Fornecedor::find($fornecedor->id);
        $response->assertStatus(200);
        $response->assertSee('disil');
        $response->assertSee('66.459.220/0001-96');
        $response->assertSee($fornecedorNew->endereco);
        $this->assertEquals('disil' ,$fornecedorNew->name );
        $this->assertEquals('66.459.220/0001-96' ,$fornecedorNew->cnpj);

    }

    /** @test */
    public function test_soft_delete_fornecedores() {
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        $fornecedor = factory(Fornecedor::class)->create();

        $this->get('/fornecedor/delete/'.$fornecedor->id);
        $fornecedorTest = Fornecedor::find($fornecedor->id);
        $this->assertNull($fornecedorTest);
        $this->assertNotNull(Fornecedor::withTrashed()->find($fornecedor->id)->restore());
    }

    /** @test */
    public function test_fornecedor_nao_pode_ser_deletado_se_tiver_produto() {
        $usuario = factory(User::class)->create();
        $fornecedor = factory(Fornecedor::class)->create();
        Auth::login($usuario);
        $produto = factory(Produto::class)->create(['fornecedor_id' => $fornecedor->id]);
        $response = $this->get('/fornecedor/delete/'.$fornecedor->id);
        $response->assertStatus(200);
        $this->assertNotNull(Fornecedor::find($fornecedor->id));
    }
}
