<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Fornecedor;
use App\Produto;


class ProdutosTest extends TestCase
{
    
    use DatabaseMigrations;


    /** @test */
    public function test_usuario_pode_listar_produtos() {
        $usuario = factory(User::class)->create();
        $produto = factory(Produto::class)->create();
        Auth::login($usuario);
        
        $response = $this->get('/produtos');
        $response->assertSee($produto->name);
        $response->assertSee($produto->descricao);
        $response->assertSee((string)$produto->preco);
        $response->assertSee(escapeString($produto->fornecedor->name));
        $response->assertSee((string)$produto->qtd);
    }

    /** @test */
    public function test_usuario_pode_cadastrar_produto()
    {
        $usuario = factory(User::class)->create();
        $produto = factory(Produto::class)->make();

        Auth::login($usuario);

        $response = $this->post('produtos/create', [
            'name' => $produto->name,
            'descricao' => $produto->descricao,
            'preco' => $produto->preco,
            'qtd' => $produto->qtd,
            'fornecedor' => $produto->fornecedor_id
        ]);

        $response->assertSee($produto->name);
        $response->assertSee($produto->descricao);
        $response->assertSee((string) $produto->preco);
        $response->assertSee(escapeString($produto->fornecedor->name));
        $response->assertSee((string)$produto->qtd);
    }

    /** @test */
    public function test_usuario_pode_editar_produtos() {
        $usuario = factory(User::class)->create();
        factory(Produto::class, 5)->create();    
        $produto = factory(Produto::class)->create();

        Auth::login($usuario);

        $response = $this->post('/produto/edit/'.$produto->id, [
            'name' => 'FIFA 17',
            'descricao' => 'asjdalalsk djaskdj alsdjl asdaskjd',
            'preco' => $produto->preco,
            'qtd' => 2,
            'fornecedor' => $produto->fornecedor_id
        ]);

        $produtoNew = Produto::find($produto->id);
        $this->assertEquals('FIFA 17', $produtoNew->name);
        $this->assertEquals('asjdalalsk djaskdj alsdjl asdaskjd', $produtoNew->descricao);
        $this->assertEquals(2, $produtoNew->qtd);

        $response->assertSee('FIFA 17');
        $response->assertSee('asjdalalsk djaskdj alsdjl asdaskjd');
        $response->assertSee((string) round($produtoNew->preco,2));
        $response->assertSee(escapeString($produtoNew->fornecedor->name));
        $response->assertSee((string)$produtoNew->qtd);
    }

    /** @test */
    public function test_usuario_pode_buscar_produto() {
        $produto = factory(Produto::class)->create();
        $usuario = factory(User::class)->create();
        Auth::login($usuario);
        
        $response = $this->get('consulta/produto?find='.$produto->name);    
        
        $response->assertSee($produto->name);
        $response->assertSee($produto->descricao);
        $response->assertSee((string) $produto->preco);
        $response->assertSee(escapeString($produto->fornecedor->name));
        $response->assertSee((string)$produto->qtd);
    }

    /** @test */
    public function test_usuario_busca_produtos_por_fornecedor() {
        $fornecedor = factory(Fornecedor::class)->create();
        
        $produtos = factory(Produto::class, 5)->create(['fornecedor_id' => $fornecedor->id]);
        $usuario = factory(User::class)->create(['tipo' => 1]);
         
        Auth::login($usuario);
        
        $response = $this->get('/consulta/produto?fornecedor='.$fornecedor->id);

        foreach($produtos as $produto) {
            $response->assertSee($produto->name);
            $response->assertSee($produto->descricao);
            $response->assertSee((string) $produto->preco);
            $response->assertSee(escapeString($produto->fornecedor->name));
            $response->assertSee((string)$produto->qtd);
        }
    }
}