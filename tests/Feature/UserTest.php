<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Produto;


class UserTest extends TestCase
{
    use DatabaseMigrations;


    /** @test */
    public function test_admin_pode_listar_todos_os_usuarios() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), "tipo" => 1]);
        
        Auth::login($usuario);
        $response = $this->get('/usuarios');
        
        $response->assertSee(escapeString($usuario->name));
        $response->assertSee($usuario->email);
        $response->assertSee($usuario->tipo == 1 ? "Administrador" : "Colaborador");
        $response->assertSee($usuario->status == 1 ? "Ativado" : "Desativado");
    }

    /** @test */
    public function test_usuario_comum_nao_pode_listar_todos_os_usuarios() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), "tipo" => 0]);
        Auth::login($usuario);
        $response = $this->get('/usuarios');
        $response->assertStatus(403);
    }
    
    /** @test */
    public function test_admin_pode_cadastrar_usuarios() {
        $admin = factory(User::class)->create(['password' => bcrypt("123456"), "tipo" => 1]);
        $usuario = factory(User::class)->make(["password" => "123456"]);

        Auth::login($admin);
        $response = $this->post('/users/create', [
                'name' => $usuario->name,
                'email' => $usuario->email,
                'tipo' => $usuario->tipo,
                'password' => $usuario->password,
                'status' => $usuario->status
            ]);
        
        $response->assertSee(escapeString($usuario->name));
        $response->assertSee($usuario->email);
        $response->assertSee($usuario->tipo == 1 ? "Administrador" : "Colaborador");
        $response->assertSee($usuario->status == 1 ? "Ativado" : "Desativado");
    }

    /** @test */
    public function test_usuario_comum_nao_pode_cadastrar_usuarios() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), "tipo" => 0]);
        
        Auth::login($usuario);
        $response = $this->post('/users/create', [
                'name' => $usuario->name,
                'email' => $usuario->email,
                'tipo' => $usuario->tipo,
                'password' => $usuario->password
            ]);

        $response->assertStatus(403);
    }

    /** @test */
    public function test_admin_pode_desativar_usuarios() {
        $admin = factory(User::class)->create(['password' => bcrypt("123456"), "tipo" => 1]);        
        Auth::login($admin);

        $usuario = factory(User::class)->create();

        $response = $this->post('users/edit/'.$usuario->id, [
            'name' => $usuario->name,
            'email' => $usuario->email,
            'tipo' => $usuario->tipo,
            'password' => $usuario->password,
            'status' => 0
        ]);

        $usuario = User::find($usuario->id);
        $response->assertStatus(200);
        $this->assertEquals($usuario->status, 0);
        $response->assertSee(escapeString($usuario->name));
        $response->assertSee($usuario->email);
        $response->assertSee("Desativado");
    }
    
    /** @test */
    public function test_admin_tem_acesso_valor_total_dos_produtos_dashboard() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        Auth::login($usuario);
        
        factory(Produto::class, 10)->create();
        
        $result = DB::table("produto")->select(DB::raw("sum(qtd * preco) as total"))->get();
        $total = $result[0]->total;
        $response = $this->get('/home');

        $response->assertSee($total);

    }


    /** @test */
    public function test_comum_nao_tem_acesso_valor_total_dos_produtos_dashboard() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 0]);
        Auth::login($usuario);
        
        factory(Produto::class, 10)->create();
        
        $result = DB::table("produto")->select(DB::raw("sum(qtd * preco) as total"))->get();
        $total = $result[0]->total;
        $response = $this->get('/home');

        $response->assertDontSee($total);

    }

    /** @test */
    public function test_usuario_pode_fazer_logout() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456")]);
        Auth::login($usuario);
        $this->get('/logout');
        $this->assertFalse(Auth::check());
    }
}
