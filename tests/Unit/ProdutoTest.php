<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Produto;
use App\Fornecedor;

class ProdutoTest extends TestCase
{

    use DatabaseMigrations;

    /** @test */
    public function test_produto_possui_fornecedor()
    {
        $produto = factory(Produto::class)->create();

        $this->assertInstanceOf(Fornecedor::class, $produto->fornecedor);
    }
}
