<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Fornecedor;
use App\Produto;

class FornecedorBrowserTest extends DuskTestCase
{

    /** @test */
    public function test_cadastrar_fornecedor_form() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $fornecedor = factory(Fornecedor::class)->make();

        $this->browse(function ($browser) use ($usuario, $fornecedor) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Fornecedores')
                ->clickLink('Fornecedores')
                ->assertSeeLink('Cadastrar Fornecedor')
                ->clickLink('Cadastrar Fornecedor')
                ->assertPathIs('/fornecedor/create')
                ->type('name', $fornecedor->name)
                ->type('cnpj', $fornecedor->cnpj)
                ->type('endereco', $fornecedor->endereco)
                ->press('Create')
                ->assertPathIs('/fornecedor/create')
                ->assertSee($fornecedor->name)
                ->assertSee($fornecedor->cnpj)
                ->assertSee($fornecedor->endereco)
                ->assertSee('Fornecedor cadastrado com sucesso');
        });
    }

    /** @test */
    public function test_edita_fornecedor_form() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $fornecedor = factory(Fornecedor::class)->create();

        $this->browse(function ($browser) use ($usuario, $fornecedor) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Fornecedores')
                ->clickLink('Fornecedores')
                ->assertSeeLink($fornecedor->id)
                ->clickLink($fornecedor->id)
                ->assertPathIs('/fornecedor/edit/'.$fornecedor->id)
                ->type('endereco', 'Novo endereco')
                ->press('Update')
                ->assertPathIs('/fornecedor/edit/'.$fornecedor->id)
                ->assertInputValue('endereco', 'Novo endereco');
        });
    }
    
    /** @test */
    public function test_deleta_fornecedor_link() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $fornecedor = factory(Fornecedor::class)->create();

        $this->browse(function ($browser) use ($usuario, $fornecedor) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Fornecedores')
                ->clickLink('Fornecedores')
                ->click('#deleta_'.$fornecedor->id)
                ->assertSee('Fornecedor deletado com sucesso') 
                ->assertDontSee($fornecedor->cnpj);
        });
    }

    /** @test */
    public function test_form_busca_fornecedor() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $fornecedor = factory(Fornecedor::class)->create();
        
        $this->browse(function ($browser) use ($usuario, $fornecedor){
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Fornecedores')
                ->clickLink('Fornecedores')
                ->type('find', $fornecedor->name)
                ->press('Buscar')
                ->assertSee($fornecedor->id)
                ->assertSee($fornecedor->name)
                ->assertSee($fornecedor->cnpj);
        });
    }

    /** @test */
    public function test_alert_fornecedor_tem_produtos_cadastrados() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $fornecedor = factory(Fornecedor::class)->create();
        factory(Produto::class)->create(['fornecedor_id' => $fornecedor->id]);

        $this->browse(function ($browser) use ($usuario, $fornecedor){
            $browser->loginAs($usuario)
                ->visit('/fornecedores')
                ->click('#deleta_'.$fornecedor->id)
                ->assertSee('O fornecedor possui produtos cadastrados')
                ->assertSee($fornecedor->cnpj);
        });
    }
}
