<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginTest extends DuskTestCase
{
    

    /** @test */
    public function test_usuario_pode_realizar_login() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456")]);
        
        $this->browse(function ($browser) use ($usuario){
            $browser->visit('/login')
                ->assertSee('Login')
                ->type('email', $usuario->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
}
