<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Fornecedor;
use App\Produto;

class ProdutoBrowserTest extends DuskTestCase
{

    /** @test */
    public function test_cadastra_produto_form() {
        $usuario = factory(User::class)->create();
        $fornecedor = factory(Fornecedor::class)->create();
        $produto = factory(Produto::class)->make(['fornecedor_id' => '']);
        
        $this->browse(function ($browser) use ($usuario, $fornecedor, $produto) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Produtos')
                ->clickLink('Produtos')
                ->assertSeeLink('Cadastrar Produto')
                ->clickLink('Cadastrar Produto')
                ->type('name', $produto->name)
                ->type('descricao', $produto->descricao)
                ->type('preco', $produto->preco)
                ->type('qtd', $produto->qtd)
                ->select('fornecedor', $fornecedor->id)
                ->press('Create')
                ->assertSee('Produto cadastrado com sucesso')
                ->assertSee($produto->name)
                ->assertSee($produto->descricao)
                ->assertSee($produto->preco)
                ->assertSee($produto->qtd)
                ->assertSee($fornecedor->name);
        });
    }

    /** @test */
    public function test_edita_produto_form() {
        $usuario = factory(User::class)->create();
        $fornecedor = factory(Fornecedor::class)->create();
        $produto = factory(Produto::class)->create(['fornecedor_id' => $fornecedor->id]);
        $newProduto = factory(Produto::class)->make();

        $this->browse(function ($browser) use ($usuario, $fornecedor, $produto, $newProduto) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Produtos')
                ->clickLink('Produtos')
                ->assertSeeLink($produto->id)
                ->clickLink($produto->id)
                ->type('name', $newProduto->name)
                ->type('descricao', $newProduto->descricao)
                ->type('preco', $newProduto->preco)
                ->type('qtd', $newProduto->qtd)
                ->select('fornecedor', $fornecedor->id)
                ->press('Update')
                ->assertSee('Produto alterado com sucesso')
                ->assertInputValue('name', $newProduto->name)
                ->assertInputValue('descricao', $newProduto->descricao)
                ->assertInputValue('preco', $newProduto->preco)
                ->assertInputValue('qtd', $newProduto->qtd)
                ->assertSelected('fornecedor', $fornecedor->id);
        });
    }


    /** @test */
    public function test_produto_com_qtd_zero_aparece_separado() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $produtos = factory(Produto::class, 5)->create(['qtd' => 0]);

        $this->browse(function ($browser) use ($usuario, $produtos) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Produtos')
                ->clickLink('Produtos')
                ->assertPathIs('/produtos')
                ->assertSee('Tabela dos Produtos com quantidade igual a 0');
                $browser->with('#TableQtdZero', function ($table) use ($produtos) {
                    foreach ($produtos as $produto) {
                        $table->assertSee($produto->descricao)
                            ->assertSee($produto->preco)
                            ->assertSee($produto->fornecedor->name)
                            ->assertSee((string)$produto->qtd);
                    }
                });
        });
    }

    /** @test */
    public function test_buscar_produto_form() {
        $usuario = factory(User::class)->create();
        $produto = factory(Produto::class)->create();

        $this->browse(function ($browser) use ($usuario, $produto) {
            $browser->loginAs($usuario)
                ->visit('/produtos')
                ->type('find', $produto->name)
                ->press('Buscar')
                ->assertSee($produto->id)
                ->assertSee($produto->name)
                ->assertSee($produto->descricao)
                ->assertSee($produto->preco)
                ->assertSee((string)$produto->qtd)
                ->assertSee($produto->fornecedor->name);
        });
    }

    /** @test*/
    public function test_buscar_produto_por_fornecedor() {
        $usuario = factory(User::class)->create();
        $fornecedor = factory(Fornecedor::class)->create();
        $produtos = factory(Produto::class, 3)->create(['fornecedor_id' => $fornecedor->id]);
        $otherProdutos = factory(Produto::class, 3)->create();

        $this->browse(function ($browser) use ($usuario, $produtos, $fornecedor, $otherProdutos){
            $browser->loginAs($usuario)
                ->visit('/produtos')
                ->select('fornecedor', $fornecedor->id)
                ->press('Buscar');

            foreach($produtos as $produto) {
                $browser->assertSee($produto->id)
                    ->assertSee($produto->name)
                    ->assertSee($produto->descricao)
                    ->assertSee($produto->preco)
                    ->assertSee((string)$produto->qtd)
                    ->assertSee($produto->fornecedor->name);
            }

            foreach($otherProdutos as $produto1) {
                $browser->assertDontSee($produto1->id);
            }

        });

    }
} 
