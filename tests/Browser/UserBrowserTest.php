<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserBrowserTest extends DuskTestCase
{
    
    /** @test*/
    public function test_usuario_pode_fazer_logout() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456")]);
        $this->browse(function ($browser) use ($usuario) {
            $browser->visit('/login')
                ->assertSee('Login')
                ->type('email', $usuario->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertSeeLink($usuario->name)
                ->clickLink($usuario->name)
                ->assertSeeLink("Logout")
                ->clickLink('Logout')
                ->assertSee('Estoque');
        });
    }

    /** @test */
    public function test_cadastro_usuario_form() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $userNew = factory(User::class)->make(['password' => "123456"]);

        $this->browse(function ($browser) use ($usuario, $userNew) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Usuarios')
                ->clickLink('Usuarios')
                ->assertSeeLink('Adicionar Usuario')
                ->clickLink('Adicionar Usuario')
                ->assertPathIs('/users/create')
                ->type('name', $userNew->name)
                ->type('email', $userNew->email)
                ->type('password', $userNew->password)
                ->select('tipo', $userNew->tipo)
                ->press('Create')
                ->assertSee("Usuario cadastrado com sucesso")
                ->assertSee($userNew->name)
                ->assertSee($userNew->email)
                ->assertSee(($userNew->tipo == 1) ? "Administrador" : "Colaborador");
        });
    }

    /** @test */
    public function test_editar_usuario_form() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 1]);
        $userNew = factory(User::class)->create(['password' => "123456"]);
        $email = factory(User::class)->make()->email;
        $this->browse(function ($browser) use ($usuario, $userNew, $email) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertSeeLink('Usuarios')
                ->clickLink('Usuarios')
                ->assertSeeLink($userNew->id)
                ->clickLink($userNew->id)
                ->assertPathIs('/users/edit/'.$userNew->id)
                ->type('email', $email)
                ->check('status')
                ->press('Update')
                ->assertInputValue('email', $email)
                ->assertSee('Usuario Desativado');
        });
    }

    
    /** @test */
    public function test_usuario_comum_nao_pode_ver_link_para_usuarios() {
        $usuario = factory(User::class)->create(['password' => bcrypt("123456"), 'tipo' => 0]);
        $this->browse(function ($browser) use ($usuario) {
            $browser->loginAs($usuario)
                ->visit('/home')
                ->assertDontSeeLink('/usuarios');
        });
    }
 }
