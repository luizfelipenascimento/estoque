<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('home', 'HomeController@index');

//User Routes
Route::get('/usuarios', 'UserController@index'); 

Route::post('/users/create', 'UserController@store');
Route::get('/users/create', 'UserController@showCreateForm');

Route::post('/users/edit/{id}', 'UserController@edit');
Route::get('/users/edit/{id}', 'UserController@showEditForm');

//Fornecedor routes
Route::post('/fornecedor/create', 'FornecedorController@store');
Route::get('/fornecedor/create', 'FornecedorController@showCreateForm');

Route::get('/fornecedor/delete/{id}', 'FornecedorController@deleteFornecedor');

Route::get('/fornecedores', 'FornecedorController@index');

Route::post('/fornecedores/{find}', 'FornecedorController@search');
Route::get('/fornecedores/{find}', 'FornecedorController@search');
Route::get('/consulta/fornecedor', 'FornecedorController@search');


Route::post('/fornecedor/edit/{id}', 'FornecedorController@edit');
Route::get('/fornecedor/edit/{id}', 'FornecedorController@showEditForm');

//Produto routes
Route::get('/produtos', 'ProdutoController@index');

Route::post('/produtos/create', 'ProdutoController@store');
Route::get('/produtos/create', 'ProdutoController@showCreateForm');

Route::post('/produto/edit/{id}', 'ProdutoController@edit');
Route::get('/produto/edit/{id}', 'ProdutoController@showEditForm');


Route::get('consulta/produto', 'ProdutoController@search');